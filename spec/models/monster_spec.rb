# == Schema Information
#
# Table name: monsters
#
#  id      :integer          not null, primary key
#  name    :string(255)
#  level   :integer
#  hp      :integer
#  exp     :integer
#  jexp    :integer
#  atk     :integer
#  def     :integer
#  mdef    :integer
#  str     :integer
#  agi     :integer
#  vit     :integer
#  int     :integer
#  dex     :integer
#  luk     :integer
#  scale   :integer
#  race    :integer
#  element :integer
#

require 'rails_helper'

RSpec.describe Monster, type: :model do
end
