namespace :test do
  desc 'test NN'
  task :nn => :environment do

   require 'ruby_fann/neurotica' #только для graphviz

    #объявляется ИНС
    fann = RubyFann::Standard.new(
    	:num_inputs=>2, #входы
    	#кол-во нейронов на первом и
            #втором уровнях соответственно
    	:hidden_neurons=>[3, 2],
    	:num_outputs=>1 #выходы
    )

    #обучение
    pairs=[[0,0],[1,0],[0,1],[1,1]] #учебные данные
    training_data = RubyFann::TrainData.new(
  	:inputs=>pairs,
  	#правильные результаты в соответствии с учебными данными
  	:desired_outputs=>[[0],[1],[1],[0]]
    )

    #собственно само обучение
    fann.train_on_data(
    	training_data, #данные для обучения
    	1000, #макс. кол-во эпох
    	1, #кол-во эпох спустя которые выводить рез-тат
    	0.001 #допустимая погрешность
    )

    #проверка обученности сети
    pairs.each do |pair|
	    puts "#{pair}: #{fann.run(pair)}"
    end

    #вывод полученных нейронов
    fann.get_neurons.each {|n| p n}

    #вывод графической схемы
    graph=RubyFann::Neurotica.new()
    graph.graph(fann, 'xor_nn.png')
  end
end
