namespace :fight do
  desc 'Fight NN'
  task nn: :environment do
    require 'ruby_fann/neurotica' # только для graphviz

    # Neural Network
    neural_network = RubyFann::Standard.new(
      num_inputs: 3,
      hidden_neurons: [5],
      num_outputs: 4)

    # Training data
    data = [
      [0.5, 1, 1], # 50% HP, gun, enemies
      [0.9, 1, 2], # 90% HP, gun, enemies
      [0.8, 0, 1], # 80% HP, no gun, enemies
      [0.3, 1, 1], # 30% HP, gun, enemies
      [0.6, 1, 2], # 60% HP, gun, enemies
      [0.4, 0, 1], # 40% HP, no gun, enemies
      [0.9, 1, 7], # 90% HP, gun, enemies
      [0.5, 1, 4], # 50% HP, gun, enemies
      [0.1, 0, 1], # 10% HP, no gun, enemies
      [0.6, 1, 0], # 60% HP, gun, enemies
      [1.0, 0, 0]  # 100% HP, no gun, enemies
    ]

    # Train on data
    training_data = RubyFann::TrainData.new(
      inputs: data,
      # Desired outputs
      desired_outputs: [
        [1, 0, 0, 0], # Fight
        [1, 0, 0, 0], # Fight
        [1, 0, 0, 0], # Fight
        [0, 1, 0, 0], # Hide
        [0, 1, 0, 0], # Hide
        [0, 1, 0, 0], # Hide
        [0, 0, 1, 0], # Wait
        [0, 0, 1, 0], # Wait
        [0, 0, 1, 0], # Wait
        [0, 0, 0, 1], # Do nothing
        [0, 0, 0, 1]  # Do nothing
      ]
    )

    training_data.save('verify.train')
    training_data = RubyFann::TrainData.new(filename: 'verify.train')

    # Learning
    neural_network.train_on_data(
      training_data,
      100_000, # count of epochs
      1, # number of periods after which output the result
      0.001 # desired MSE (mean-squared-error)
    )

    # Save neural network
    neural_network.save('neural_network.net')



    # Neurons list
    neural_network.get_neurons.each { |n| p n }

    # Graph
    graph = RubyFann::Neurotica.new
    graph.graph(neural_network, 'fight.png')
  end
end
