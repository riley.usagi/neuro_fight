Rails.application.routes.draw do
  root 'monsters#index'

  get '/testing',    to: 'monsters#testing',          as: :testing
  get '/training',   to: 'monsters#training_process', as: :training
end
