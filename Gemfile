source 'https://rubygems.org'

ruby '2.3.1'

gem 'rails', '4.2.6'

# Servers
gem 'puma'
gem 'unicorn'

# Auth
# gem 'devise'
# gem 'devise-i18n'
gem 'cancancan'
gem 'httparty'
gem 'oauth2'

# gem 'omniauth'
# gem 'omniauth-facebook'
# gem 'omniauth-vkontakte'
# gem 'omniauth-github'
# gem 'omniauth-twitter'
# gem 'omniauth-google-oauth2'
# gem 'omniauth-googleplus'
# gem 'omniauth-soundcloud'

# Admin Panel
# gem 'rails_admin', '0.6.5'
# gem 'rails_admin_flatly_theme', github: 'konjoot/rails_admin_flatly_theme'

# Data
gem 'redis-rails'
gem 'mysql2', '~> 0.3.17'
gem 'friendly_id', '~> 5.1.0'
gem 'simple_enum'
gem 'mechanize'
# gem 'pg'
gem 'seed_dump'
# gem 'ar-octopus'

# Forms
gem 'simple_form'
# gem 'tinymce-rails'
# gem 'tinymce-rails-langs'

# Markdown
gem 'redcarpet'

# Assets
gem 'jquery-rails'
gem 'coffee-rails'
gem 'sass-rails'
gem 'uglifier'
gem 'jbuilder'
gem 'russian'
gem 'babosa'
# gem 'turbolinks'
# gem 'jquery-turbolinks'
# gem 'jquery-fileupload-rails', '0.4.1'

# Other gems
gem 'slim'
gem 'sidekiq'
gem 'whenever'
gem 'binding_of_caller'
gem 'kaminari'
gem 'bootstrap-kaminari-views'
gem 'flutie'
gem 'rails_config'
gem 'dotenv-rails' # Environment variables link: 'https://github.com/bkeepers/dotenv'
gem 'faker'
gem 'ruby-fann'
gem 'ruby-graphviz'
# gem 'github_api'
# gem 'random_username'
# gem 'humans'
# gem 'holder_rails'
# gem 'actiontimer'
# gem 'acts-as-taggable-on', '~> 3.4'
# gem 'ckeditor'
# gem 'acts_as_commentable'
# gem 'faker'
# gem "breadcrumbs_on_rails"

# Images
# gem 'paperclip'
gem 'carrierwave'
gem 'mini_magick'
gem 'rmagick'

# Deploy
gem 'capistrano', '~> 3.3.5'
gem 'capistrano-rails', '~> 1.1.2'
# gem 'capistrano-rbenv'
gem 'capistrano-rvm'
gem 'capistrano3-unicorn'

group :test do
  gem 'rspec'
  gem 'rspec-rails'
  gem 'cucumber'
  gem 'selenium-webdriver'
  gem 'capybara'
  gem 'mocha'
  gem 'factory_girl'
  gem 'factory_girl_rails'
  gem 'shoulda-matchers'
  gem 'guard-jasmine'
end

group :development do
  gem 'guard-rspec', require: false
  gem 'better_errors'
  gem 'meta_request'
  gem 'annotate'
  gem 'pry-rails'
end

group :production do
  gem 'rails_12factor'
  gem 'google-analytics-rails'
end

# group :doc do
#   gem 'sdoc', require: false
# end
