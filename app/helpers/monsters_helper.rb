# == Schema Information
#
# Table name: monsters
#
#  id      :integer          not null, primary key
#  name    :string(255)
#  level   :integer
#  hp      :integer
#  exp     :integer
#  jexp    :integer
#  atk     :integer
#  def     :integer
#  mdef    :integer
#  str     :integer
#  agi     :integer
#  vit     :integer
#  int     :integer
#  dex     :integer
#  luk     :integer
#  scale   :integer
#  race    :integer
#  element :integer
#

# Monsters helper
module MonstersHelper
  def fight_result?(hero, monster)
    neural_network =
      RubyFann::Standard.new(filename: 'public/neural_network.net')
    if neural_network.run(
      [hero.hp, hero.atk, monster.hp, monster.atk]
    ).first.to_f > 0.5
      'Fight'
    else
      'Runaway!'
    end
  end
end
