# Monsters controller
class MonstersController < ApplicationController
  require 'ruby_fann/neurotica'

  # Main output
  def index
    @user     = User.last
    @monsters = Monster.in_range(@user.level)
  end

  def testing
    neural_network =
      RubyFann::Standard.new(filename: 'public/neural_network.net')
    render plain: neural_network.run([1000, 25, 50, 7])
  end

  # Training process
  def training_process
    train_on_data(neural_network, training_data)
    redirect_to root_path, flash: { result: 'Training complete' }
  end

  private

  # Create training data for teaching neural network
  def training_data
    data # => [[1000, 25, 50, 7], [1000, 25, 169, 22],...]
    training_data = RubyFann::TrainData.new(
      inputs: @data,
      desired_outputs: desired_outputs(@data)
    )
    training_data.save('public/verify.train')
  end

  # Prepare data via fight calculation
  def data
    main_characters
    @data = []
    # Fight part
    @monsters.each do |monster|
      # pair = []
      # pair <<
      @data << [@hero.hp, @hero.atk, monster.hp, monster.atk]
    end
    @data
    # raise main_characters.inspect
  end

  # Prepare hero and monsters data
  def main_characters
    @hero     = User.last
    # @monsters = Monster.in_range(@hero.level)
    @monsters = Monster.all
  end

  # Desired outputs using pairs of hero and monster fight results
  def desired_outputs(pairs)
    @desired_output = []
    pairs.each do |pair|
      hero_hp     = pair[0]
      hero_atk    = pair[1]
      monster_hp  = pair[2]
      monster_atk = pair[3]

      # Init fight
      fight(hero_hp, hero_atk, monster_hp, monster_atk)
    end
    @desired_output
  end

  def fight(hero_hp, hero_atk, monster_hp, monster_atk)
    while hero_hp >= 0 && monster_hp >= 0
      monster_hp -= hero_atk
      hero_hp    -= monster_atk
    end

    @desired_output << if hero_hp >= 0 && monster_hp <= 0
                         [1]
                       else
                         [0]
                       end
  end

  # Learning
  def train_on_data(network, training_data)
    network.train_on_data(
      training_data,
      1_000_000, # count of epochs
      1000,   # number of periods after which output the result
      0.01    # desired MSE (mean-squared-error)
    )

    # Save neural network
    network.save('public/neural_network.net')

    # Graph
    graph = RubyFann::Neurotica.new
    graph.graph(neural_network, 'public/fight.png')
  end

  # Init neural network
  def neural_network
    RubyFann::Standard.new(
      filename: 'public/neural_network.net',
      num_inputs: 4,
      hidden_neurons: [5, 3, 1, 3, 5],
      num_outputs: 1
    )
  end
end
