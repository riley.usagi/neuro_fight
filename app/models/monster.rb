# == Schema Information
#
# Table name: monsters
#
#  id      :integer          not null, primary key
#  name    :string(255)
#  level   :integer
#  hp      :integer
#  exp     :integer
#  jexp    :integer
#  atk     :integer
#  def     :integer
#  mdef    :integer
#  str     :integer
#  agi     :integer
#  vit     :integer
#  int     :integer
#  dex     :integer
#  luk     :integer
#  scale   :integer
#  race    :integer
#  element :integer
#

# Monster model
class Monster < ActiveRecord::Base
  def self.in_range(hero_level)
    where('level between ? and ?', hero_level - 20, hero_level + 20)
  end
end
