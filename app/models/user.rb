# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  username   :string(255)
#  gold       :integer          default(0)
#  level      :integer          default(1)
#  hp         :integer          default(1000)
#  exp        :integer          default(0)
#  atk        :integer          default(25)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class User < ActiveRecord::Base
end
