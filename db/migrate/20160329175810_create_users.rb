class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.integer :gold, default: 0
      t.integer :level, default: 1
      t.integer :hp, default: 1000
      t.integer :exp, default: 0
      t.integer :atk, default: 25
      t.timestamps null: false
    end
  end
end
