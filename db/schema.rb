# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160329175810) do

  create_table "monsters", force: :cascade do |t|
    t.string  "name",    limit: 255
    t.integer "level",   limit: 4
    t.integer "hp",      limit: 4
    t.integer "exp",     limit: 4
    t.integer "jexp",    limit: 4
    t.integer "atk",     limit: 4
    t.integer "def",     limit: 4
    t.integer "mdef",    limit: 4
    t.integer "str",     limit: 4
    t.integer "agi",     limit: 4
    t.integer "vit",     limit: 4
    t.integer "int",     limit: 4
    t.integer "dex",     limit: 4
    t.integer "luk",     limit: 4
    t.integer "scale",   limit: 4
    t.integer "race",    limit: 4
    t.integer "element", limit: 4
  end

  create_table "users", force: :cascade do |t|
    t.string   "username",   limit: 255
    t.integer  "gold",       limit: 4,   default: 0
    t.integer  "level",      limit: 4,   default: 1
    t.integer  "hp",         limit: 4,   default: 1000
    t.integer  "exp",        limit: 4,   default: 0
    t.integer  "atk",        limit: 4,   default: 25
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

end
